A quick note about my submission for the re-sit

I tried my best to follow your advice from my first submission and did what I could to rectify any programming mistakes or examples of bad practise within the program. 

In my report I also took your advice to go more in depth with my discussions of memory and lists to show better understanding and a more competent level of study for the module this time around.

I hope the re-sit reflects good enough improvement on my original submission to collect the credits for this module.
