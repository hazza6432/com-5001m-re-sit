#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//Building structs=================================================================================
typedef struct PokemonNode{//PokemonNode struct. Typedef used to define struct as a data type
    int pokenum;// pokenum is the pokedex number for the pokemon
    char name[30];//pokemon's name
    char type[15];//pokemon's type
    char ability[30];//Pokemon's ability
    struct PokemonNode *next;//pointer to next node in list when added

}PokemonNode;

typedef struct PlayerNode{//PlayerNode struct. Typedef used to define struct as a data type
    
    char name[15];//Player's name
    int ID;//player's pokedex number
    int count;//count of pokemon
    
    struct PlayerNode *next;//Pointer to next player in list. 
    
    }PlayerNode;

typedef struct pokedex{//pokedex struct

  PokemonNode *pkmnhead;//pointer to head of pokemon list
  PlayerNode *plyrhead;//pointer to head of player list
  PlayerNode *displayhead;
  int pkmncount;
  int plyrcount;


}pokedex;
//Building structs END =================================================================================



//functions (pokemon) ==========================================================================================
PokemonNode* NewPokemonNode(int pokeNum,char Name[30],char Type[15],char Ability[30]){
  PokemonNode *NewPokemonNode = NULL;               
  NewPokemonNode =malloc(sizeof(PokemonNode));//allocates memory for new node   
  if (NewPokemonNode != NULL)//checks memory space has been created
  {
    NewPokemonNode->pokenum = pokeNum;//adding pokemon data into node
    strcpy(NewPokemonNode->name,Name);
    strcpy(NewPokemonNode->type,Type);
    strcpy(NewPokemonNode->ability,Ability);
    NewPokemonNode->next = NULL;
  }//sets next node to be empty and returns pokemon node to function caller
  
  return NewPokemonNode;
}

void *startlist(PokemonNode **Startlist,int pokeNum,char Name[30],char Type[15],char Ability[30]){
  PokemonNode *temp = NewPokemonNode(pokeNum,Name,Type,Ability);//Calls the function on line 41 to create a new node
  temp->next = *Startlist;//assigns pointer that points to the node that was already in the list 
  *Startlist = temp;
}


void AddtopkmnList (PokemonNode **Begin, int pokeNum, char Name[30], char Type[15], char Ability[30]){//function using Startlist to add pokemon to List
  PokemonNode *temp = *Begin;//sets temp variable of type PokemonNode to start at beginning of list
  PokemonNode *last = NULL;//sets pointer to previous node to null as there isn't one at this stage
  PokemonNode *newPokemon = NewPokemonNode(pokeNum, Name, Type, Ability);//sets variable newpokemon for the new data to be stored in
  if (temp == NULL){//sets the startlist function to begin working if temp is empty
    startlist(Begin,pokeNum, Name, Type, Ability);
  }
  else{
    while (temp != NULL){//while iterating through list;
      if (temp->pokenum == pokeNum)//is the given pokedex number is the same as one already in the list;
      {//Tell the user it already exists
        printf("You have already got a record of this Pokemon!\n",temp->name);
        return;
      }
      
      last = temp;//sets the previous node in the list to the one currently defined by temp
      temp = temp->next;//sets temp to the next node in the list
     //effectively shifting the current node back one space while moving the forthcoming node into the current held "temp" slot
    }
    last->next = newPokemon;
    
  }
  
}
//functions (pokemon) END ==========================================================================================





//functions (player)===================================================================================
PlayerNode* NewPlayerNode(int id,char Name[15], int Count){
  PlayerNode *NewPlayerNode = NULL;               
  NewPlayerNode =malloc(sizeof(PlayerNode));//allocates memory for new node    
  if (NewPlayerNode != NULL)//checks space has been created       
  {//setting supplied data into variables within node
    NewPlayerNode->ID = id;
    strcpy(NewPlayerNode->name,Name);
    NewPlayerNode->count = Count;
    NewPlayerNode->next = NULL;
  }
  
  return NewPlayerNode;// return node to function caller
}

void *begiN(PlayerNode **BEGIN,int id,char Name[15], int Count) {//same as startlist function using different variable name to help differentiate
  PlayerNode *temp = NewPlayerNode(id, Name, Count);//uses different parameters to PokemonNode
  temp->next = *BEGIN;//assigns pointer that points to the node that was already in the list
  *BEGIN = temp;
}



void AddtoplyrList (PlayerNode **BEGIN, int id, char Name[15],int Count) {
  PlayerNode *temp = *BEGIN;//sets temp variable of type PokemonNode to start at beginning of list
  PlayerNode *last = NULL;//sets pointer to previous node to null as there isn't one at this stage
  PlayerNode *newPlayer = NewPlayerNode(id, Name, Count);//creates variable newPlayer for this data to be stored in
  if (temp == NULL){//sets begiN function to work if temp is empty
    begiN(BEGIN, id, Name, Count);
  }
  else{//checks each node to make sure it doesn' already exist in the list
    while (temp != NULL){
      if (temp->ID == id)
      {
        printf("You have already got a record of this Player!\n",temp->name);
        return;
      }
      
      last = temp;//sets the previous node in the list to the one currently defined by temp
      temp = temp->next;//sets temp to the next node in the list
     //effectively shifting the current node back one space while moving the forthcoming node into the current held "temp" slot
    }
    last->next = newPlayer;
    
  }
  
}
//functions (player) END ===================================================================================



//List Pokemon ==========================================================================

 void displaypkmnList (PokemonNode *beginning){

    while(beginning != NULL){//Iterates through list as long as pointers show available nodes
    printf("Pokemon Number: %d\n",beginning->pokenum);//Taking data from nodes and assigning them to output
    printf("%s",beginning->name);
    printf("\n");
    printf("%s",beginning->type);
    printf("\n");
    printf("%s",beginning->ability);
    printf("\n");
    beginning = beginning->next;
    printf("\n");
  }
}
//=======================================================================================

//List Players ==========================================================================
 void displayplyrList (PlayerNode *Beginning){
    while(Beginning != NULL){//Iterates through list as long as pointers show nodes are available
    printf("Player Number: %i\n",Beginning->ID);//Finding data from the node and assigning it to output
    printf("%s",Beginning->name);
    printf("\n");
    Beginning = Beginning->next;
    printf("\n");
  }
}
//=======================================================================================


//Display Player Details ================================================================
void DisplayPlayerDetails(PlayerNode *START , int ID){//This function iterates through player list to find a player
  printf("Pokedex Entry: %s\n", ID);//Player number is asked for in main function
 
  PlayerNode *temp = START;//creates variable temp to temporarily store a player's details as it iterates through list
  
while (temp ->next != NULL && temp->ID != ID)//sets the loop to continue as long as there are more nodes to check and as long as said nodes' ID is not the one entered
{
  temp = temp->next;//moving through list
}

  if(temp->ID == ID){//Checks if ID featured in temp matches the search query
    printf("Player Name: %s",START->name);
    printf("Player pokemon count: %d", START->count);//If there's a match, print other data attached to the node
  }
  else{
    printf("That player couldn't be found");
  }
  
}

int pokedexmenu(){//simple function free of parameters used to display the man menu before or after a choice is made

printf("Welcome to the pokedex, trainer! \n");
printf("--------------------------------");
printf("Hit 1 to Show all pokemon!");
printf("Hit 2 to Show all pokemon!");
printf("Hit 3 to Show all pokemon!");

};


//main function====================================================================================================
int main(void){

pokedex *run;//sets variable run of type pokedex to operate with the data type
run->pkmnhead = NULL;//accessing heads of lists and setting to NULL
run -> plyrhead = NULL;
run-> displayhead = NULL;
PokemonNode *head = run->pkmnhead;//making sure the list is empty
PlayerNode *HEAD = run-> displayhead;

int choice;//for switch used later
int pick;//for switch used later

pokedexmenu();//calls main menu function
scanf("%d", &choice);//takes the user input in response to main menu. Assigns input to choice variable.


//Adding pokemon data into pokedex=======================================================
  AddtopkmnList(&head,861,"Grimmsnarl","Dark","Spirit Break");//&&head used here to ensure node is not missed when searching
   run->pkmncount++;// necessary pieces of data used as parameters and filled in here. 
  AddtopkmnList(&head,26,"Raichu","Electric","Electro Ball");
   run->pkmncount++;
   AddtopkmnList(&head,536,"Palpitoad","Water","Bubble beam");
   run->pkmncount++;
   AddtopkmnList(&head,655,"Delphox","Fire","Flame Charge");
   run->pkmncount++;
   AddtopkmnList(&head,370,"Luvdisc","Water","Hydro Pump");
   run->pkmncount++;
   AddtopkmnList(&head,187,"Hoppip","Grass","Bullet Seed");
   run->pkmncount++;
   AddtopkmnList(&head,3,"Venusaur","Grass","Vine whip");
   run->pkmncount++;
   AddtopkmnList(&head,345,"Lileep","Rock","Confuse Ray");
   run->pkmncount++;
   AddtopkmnList(&head,485,"Heatran","Steel","Ancient Power");
   run->pkmncount++;
   AddtopkmnList(&head,678,"Meowstic","Psychic","Psybeam");
   run->pkmncount++;
   AddtopkmnList(&head,848,"Toxel","Poison","Belch");
   run->pkmncount++;
   AddtopkmnList(&head,662,"Fletchinder","Flying","Peck");
   run->pkmncount++;
//Adding pokemon data into pokedex=======================================================

 



//Adding pokemon data into pokedex=======================================================
PlayerNode *Head = run->plyrhead;

   AddtoplyrList(&Head,1, "Harry", 480);//4 parameters here - Head so that the node can stand out in the list
   run->plyrcount++;//other 3 parameters for data to be filled into node
   AddtoplyrList(&Head,2, "Will", 214);
   run->plyrcount++;
   AddtoplyrList(&Head,3, "Emma", 26);
   run->plyrcount++;
   AddtoplyrList(&Head,4, "CJ", 5);
   run->plyrcount++;
   AddtoplyrList(&Head,5, "Lucille", 11);
   run->plyrcount++;
   AddtoplyrList(&Head,6, "Joe", 67);
   run->plyrcount++;
   AddtoplyrList(&Head,7, "Bella", 99);
   run->plyrcount++;
   AddtoplyrList(&Head,8, "Rick", 2);
   run->plyrcount++;
   AddtoplyrList(&Head,9, "Josh", 40);
   run->plyrcount++;
   AddtoplyrList(&Head,10, "Charlie", 372);
   run->plyrcount++;
   AddtoplyrList(&Head,11, "Leonardo", 204);
   run->plyrcount++;
   AddtoplyrList(&Head,12, "Dan", 3);
   run->plyrcount++;
//Adding pokemon data into pokedex=======================================================





//Switch to provide instructions based on instances set by user input
    switch(choice){
        case 1: 
            
            displaypkmnList(head);//Displays pokemon List
            

        case 2: 
            displayplyrList(Head);//Displays Player list
            

      
          
        case 3: 
        printf("Choose a player Number!");
        scanf("%d", &pick);
           DisplayPlayerDetails(HEAD, pick);//Takes a player number and displays their corresponding details


  default:
  printf("Sorry, That won't work!");//Default case for if switch gets confused
  pokedexmenu();

};

};

//End of Program